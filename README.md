# Team 14

## Topic 101:

### How consistent are the implementations of machine learning algorithms in different ML libraries?

Many machine learning algorithms such as linear regression or neural networks are implemented by various machine learning frameworks such as scikit-learn, TensorFlow and Weka. However, minor changes in implementations (e.g. different default hyper parameters) may sometimes lead to significant changes in the performance of machine learning algorithms. Hence, your task is to answer the question whether implementations of the same algorithms in different frameworks producing the same results

## Libraries

### Scikit-Learn

#### [Scikit Learn](http://scikit-learn.org/stable/): a Machine Learning in Python

* Simple and efficient tools for data mining and data analysis

* Accessible to everybody, and reusable in various contexts

* Built on NumPy, SciPy, and matplotlib

* Open source, commercially usable - BSD license

### TensorFlow

#### [TensorFlow](https://www.tensorflow.org/): An open source machine learning framework for everyone

TensorFlow™ is an open source software library for high performance numerical computation. Its flexible architecture allows easy deployment of computation across a variety of platforms (CPUs, GPUs, TPUs), and from desktops to clusters of servers to mobile and edge devices. Originally developed by researchers and engineers from the Google Brain team within Google’s AI organization, it comes with strong support for machine learning and deep learning and the flexible numerical computation core is used across many other scientific domains.

## Algorithms

### Linear Regression (Ayush Ghai)

### Logistic Regression (Kavish Bhathija)

### Support Vector Machine & Kernel Function (Yifan Xu)

## Report

Please beware of using the link below to write our report, please. [Google Doc](https://docs.google.com/document/d/1GM0OcCC8JwTrg64CX9HDQpbsSn6YFGxjJP5kWo7BeTc/edit?usp=sharing)
