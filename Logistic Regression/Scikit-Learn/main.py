#!/usr/bin/python2

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model, preprocessing, metrics
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score
from matplotlib.colors import ListedColormap

def plot_decision_regions(X, y, classifier, test_idx=None, resolution=0.001):
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    # plot class samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1],alpha=0.8, c=cmap(idx),marker=markers[idx], label=cl)
    # highlight test samples
    if test_idx:
        X_test, y_test = X[test_idx, :], y[test_idx]
        plt.scatter(X_test[:, 0], X_test[:, 1], c='', alpha=1.0, linewidth=1, marker='o', s=55, label='test set')

if __name__ == '__main__':
    specy_dict = {'setosa': 0, 'versicolor' : 1, 'virginica' : 2}
    color_dict = {0 : 'red', 1 : 'green', 2 : 'blue'}

    dataset = pd.read_csv('../Dataset/iris.csv')

    X = dataset[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']].values
    Y = dataset.replace({'species': specy_dict})['species'].values

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1)

    # Encode
    labelEncoder = preprocessing.LabelEncoder()
    Y_train_encoded = labelEncoder.fit_transform(Y_train.ravel())

    logisticRegr = linear_model.LogisticRegression(max_iter=1000)
    logisticRegr.fit(X_train, Y_train_encoded)

    Y_pred = logisticRegr.predict(X_train)

    # plot_decision_regions(X, Y, classifier=logisticRegr, test_idx=range(0, 150))

    # plt.scatter(X_test[:, 0], X_test[:, 1], color=np.where(Y_pred == 1, 'blue', 'red'))
    plt.scatter(X_train[:, 0], X_train[:, 1], color=pd.DataFrame(data={'species' : Y_pred}).replace({'species' : color_dict}).values.flatten())
    plt.xlabel('Sepal Length')
    plt.ylabel('Sepal Width')

    plt.xticks(())
    plt.yticks(())

    plt.show()
