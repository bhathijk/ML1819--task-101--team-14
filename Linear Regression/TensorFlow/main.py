#!/usr/bin/python3

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
rng = np.random

if __name__ == '__main__':
    # Parameters
    learning_rate = 0.01
    training_epochs = 1
    display_step = 50

    # Training Data

    with open('../Dataset/train.csv') as train_dataset_file:
        train_dataset_file.readline()
        train_dataset = np.loadtxt(train_dataset_file, delimiter=',')

    train_dataset_file.close()

    with open('../Dataset/test.csv') as test_dataset_file:
        test_dataset_file.readline()
        test_dataset = np.loadtxt(test_dataset_file, delimiter=',')

    test_dataset_file.close()

    X_train = train_dataset[:, :1]
    X_test = test_dataset[:, :1]

    Y_train = train_dataset[:, 1:2]
    Y_test = test_dataset[:, 1:2]

    # tf Graph Input
    X = tf.placeholder("float")
    Y = tf.placeholder("float")

    # Set model weights
    W = tf.Variable(rng.randn(), name="weight")
    b = tf.Variable(rng.randn(), name="bias")

    # Construct a linear model
    Y_pred = tf.add(tf.multiply(X, W), b)

    # Mean squared error
    cost = tf.reduce_sum(tf.pow(Y_pred - Y, 2)) / (2 * len(X_train))
    # Gradient descent
    #  Note, minimize() knows to modify W and b because Variable objects are trainable=True by default
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    # Initialize the variables (i.e. assign their default value)
    init = tf.global_variables_initializer()

    # Start training
    with tf.Session() as sess:

        # Run the initializer
        sess.run(init)

        # Fit all training data
        for epoch in range(training_epochs):
            for (x, y) in zip(X_train, Y_train):
                sess.run(optimizer, feed_dict={X: x, Y: y})

            # Display logs per epoch step
            if (epoch+1) % display_step == 0:
                c = sess.run(cost, feed_dict={X: X_train, Y:Y_train})
                print("Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(c), \
                    "W=", sess.run(W), "b=", sess.run(b))

        print("Optimization Finished!")
        training_cost = sess.run(cost, feed_dict={X: X_train, Y: Y_train})
        print("Training cost=", training_cost, "W=", sess.run(W), "b=", sess.run(b), '\n')

        # Graphic display
        # plt.plot(X_train, Y_train, 'ro', label='Original data')
        # plt.plot(X_train, sess.run(W) * X_train + sess.run(b), label='Fitted line')
        # plt.legend()
        # plt.show()

        # Testing example, as requested (Issue #2)

        print("Testing... (Mean square loss Comparison)")
        testing_cost = sess.run(
            tf.reduce_sum(tf.pow(Y_pred - Y, 2)) / (2 * X_test.shape[0]),
            feed_dict={X: X_test, Y: Y_test})  # same function as cost above
        print("Testing cost=", testing_cost)
        print("Absolute mean square loss difference:", abs(
            training_cost - testing_cost))

        plt.plot(X_test, Y_test, 'bo', label='Testing data')
        plt.plot(X_train, sess.run(W) * X_train + sess.run(b), label='Fitted line')
        plt.legend()
        plt.show()
