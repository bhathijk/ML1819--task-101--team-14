#!/usr/bin/python2

import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

def main():
    # Load the diabetes dataset
    # diabetes = datasets.load_diabetes()
    # Load the dataset from csv file
    with open('../Dataset/train.csv') as train_dataset_file:
        train_dataset_file.readline()
        train_dataset = np.loadtxt(train_dataset_file, delimiter=',')

    train_dataset_file.close()

    with open('../Dataset/test.csv') as test_dataset_file:
        test_dataset_file.readline()
        test_dataset = np.loadtxt(test_dataset_file, delimiter=',')

    test_dataset_file.close()

    X_train = train_dataset[:, :1]
    X_test = test_dataset[:, :1]

    Y_train = train_dataset[:, 1:2]
    Y_test = test_dataset[:, 1:2]

    regr = linear_model.LinearRegression()
    regr.fit(X_train, Y_train)
    Y_pred = regr.predict(X_test)

    # plt.scatter(X_train, y_train, color='black')
    plt.scatter(X_test, Y_test, color='red')
    plt.plot(X_test, Y_pred, color='blue', linewidth=3)

    plt.xticks(())
    plt.yticks(())

    plt.show()
    # # Use only one feature
    # diabetes_X = diabetes.data[:, , 2]
    #
    # # Split the data into training/testing sets
    # diabetes_X_train = diabetes_X[:-20]
    # diabetes_X_test = diabetes_X[-20:]
    #
    # # Split the targets into training/testing sets
    # diabetes_y_train = diabetes.target[:-20]
    # diabetes_y_test = diabetes.target[-20:]
    #
    # # Create linear regression object
    # regr = linear_model.LinearRegression()
    #
    # # Train the model using the training sets
    # regr.fit(diabetes_X_train, diabetes_y_train)
    #
    # # Make predictions using the testing set
    # diabetes_y_pred = regr.predict(diabetes_X_test)
    #
    # # The coefficients
    # print('Coefficients: \n', regr.coef_)
    # # The mean squared error
    # print("Mean squared error: %.2f"
    #       % mean_squared_error(diabetes_y_test, diabetes_y_pred))
    # # Explained variance score: 1 is perfect prediction
    # print('Variance score: %.2f' % r2_score(diabetes_y_test, diabetes_y_pred))
    #
    # # Plot outputs
    # plt.scatter(diabetes_X_test, diabetes_y_test,  color='black')
    # plt.plot(diabetes_X_test, diabetes_y_pred, color='blue', linewidth=3)
    #
    # plt.xticks(())
    # plt.yticks(())
    #
    # plt.show()

if __name__ == '__main__':
    main()
